# Project Management App

## About the Project

The project is a simple app that is mainly used to manage projects and tasks between Contractors, Subcontractors and Assembly teams

---

## Setup Guide

1. From the repository page, click the button labeled as Clone
2. In the drop down, select SSH
3. Copy the URL for the repository
4. On your local machine, open the bash shell and change the directory to your own working directory where you would like to clone the repository
5. Paste the link that you copied from your repository
6. Press enter, and the repository will be stored in your working directory
7. in the directory, open the `pubspec.yaml` file on your flutter folder
8. Save the file, this will automatically run the flutter pub get command and will install the packages

## Flutter Version

The flutter version used to make the project is Flutter 2.2.3 with the Dart Version 2.13.4

## Packages

[http](https://pub.dev/packages/http) ^0.13.3

- Was used to make HTTP requests

[provider](https://pub.dev/packages/provider) ^5.0.0

- Was used to make a global state for the authentication of the user, it was used to store the access token and the designation of the user

[shared_preferences](https://pub.dev/packages/shared_preferences) ^2.0.6

- Used to persist the data of the user who is logged in

[flutter_dotenv](https://pub.dev/packages/dotenv) ^5.0.0

- Used to create environment variables of the URL for the request used in the APIs

[image_picker](https://pub.dev/packages/image_picker) ^0.8.0+3

- Was used to upload an image for the detailsd

[font_awesome_flutter](https://pub.dev/packages/font_awesome_flutter) ≥ ^4.7.0

- The font awesome icons was used in creating the about the developer page

## Features of the Project

- Login Feature
    - The login feature must save token to app state
    - It must save token to local storage
- For the Contractor
    - Show all project
    - Add a project
    - Assign Project to a contractor
    - Review Finished project tasks
- For the Subcontractor
    - Show assigned projects
    - Show project tasks
    - Add tasks & assign to assembly team
- For the Assembly-team
    - Show projects with assigned tasks
    - Show project tasks and tag tasks if the task is ongoing
    - Tag task as completed