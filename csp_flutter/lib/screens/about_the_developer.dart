import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../widgets/contact_info.dart';
import '../widgets/title_info.dart';
import '../models/developer.dart';

class AboutMe extends StatelessWidget {
    
    TextFormField contactForm(String label, TextInputType keyboardType){
        return TextFormField(
            decoration: InputDecoration(labelText: label),
            keyboardType: keyboardType,
        );
    }

    @override
    Widget build(BuildContext context) {
        final mediaQuery = MediaQuery.of(context);

        Container mainHead(AppBar appBar){ 
            return Container(
                height: (mediaQuery.size.height - appBar.preferredSize.height - mediaQuery.padding.top)*0.35,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            blurRadius: 2.0,
                            spreadRadius: 0.0,
                            offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                    ],
                    gradient: LinearGradient(
                        colors: [
                            // Color.fromRGBO(205, 23, 25, 1).withOpacity(0.75),
                            Color.fromRGBO(20, 45, 68, 1),
                            Color.fromRGBO(205, 23, 25, 1),
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight
                    ),
                        borderRadius: BorderRadius.circular(15) 
                ),
                child: Column (
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                        CircleAvatar(
                            radius: 55,
                            backgroundColor: Colors.white,
                            child:Center(
                                child: CircleAvatar(
                                    radius: 50,
                                    backgroundImage: AssetImage('assets/prof_pic.jpg'),
                                ),
                            )
                        ),
                        Center(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                    Text(
                                        'Christian Andrew Zurita',
                                        style: TextStyle(
                                            fontFamily: 'Raleway',
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                            fontSize: 22
                                        ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                        'Flutter Developer',
                                        style: TextStyle(
                                            fontFamily: 'Raleway',
                                            fontSize: 18,
                                            color: Colors.white
                                        ),    
                                    ),
                                    SizedBox(height:5),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                            Icon(
                                                FontAwesomeIcons.map,
                                                size: 18,
                                                color: Colors.white
                                            ),
                                            SizedBox(width: 5,),
                                            Text(
                                                'Laguna, Philippines',
                                                style: TextStyle(
                                                    fontFamily: 'Raleway',
                                                    fontSize: 18,
                                                    color: Colors.white
                                                )
                                            )
                                        ]
                                    )
                                    
                                ],
                                
                            )
                        )
                    ],
                )
            );
        }

        Widget shortIntro = Container(
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(5),
            child: Text(
                '       Hi I\'m Don, I\'m an Electronics Engineer who wishes to have a career in the beautiful field of Tech',
                style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 18,
                    color:  Color.fromRGBO(20, 45, 68, 1)
                ),
                textAlign: TextAlign.justify,
            )
        );

        Widget forBtn = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 10.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Color.fromRGBO(205, 23, 25, 1)
                ),
                onPressed: () { 
                }, 
                child: Text('Send!')
            )
        );

        Widget mainBody(AppBar appBar) {
            return Container(
                height: 350,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(
                    bottom: 10,
                    left: 10,
                    right: 10,
                    top: 2
                ),
                decoration: BoxDecoration(
                     boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            blurRadius: 2.0,
                            spreadRadius: 0.0,
                            offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                    ],
                    borderRadius: BorderRadius.circular(15),
                    color:  Colors.grey[300],
                ),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            SizedBox(height:10),
                            shortIntro,
                            SizedBox(height:10),
                            ...Developer().listSkills,
                            SizedBox(height:15),
                            ...Developer().listExperience,
                            ...Developer().listInterest,
                            TitleInfo('Contact me'),
                            contactForm('Name', TextInputType.name),
                            contactForm('Email', TextInputType.emailAddress),
                            contactForm('Message', TextInputType.multiline),
                            forBtn,
                            ContactInfo()
                        ],
                    ),
                ),
            );
        }

        return Scaffold(
            // backgroundColor:  Color.fromRGBO(205, 23, 25, 1),
            appBar: AppBar(
                title: Text('About the Developer'),
                elevation: 0,
                backgroundColor: Color.fromRGBO(205, 23, 25, 1),
            ),
            body: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        mainHead(AppBar()),
                        mainBody(AppBar()),
                    ],
                )
            )
        );
    }
}