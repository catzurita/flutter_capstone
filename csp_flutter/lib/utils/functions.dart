import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String message) {
    SnackBar snackBar = new SnackBar(
        content: Text(message), 
        duration: Duration(milliseconds: 2000)
    );
    
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    
}

 Widget form({
            String? title, 
            TextEditingController? controller, 
            String? message,
            FocusNode? focus
}){
    return TextFormField(
        decoration: InputDecoration(labelText: title),
        keyboardType: TextInputType.text,
        controller: controller,
        onEditingComplete: focus!.nextFocus,
        validator: (value) {
            return (value != null && value.isNotEmpty) ? null : message;
        }
    );
}