import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/functions.dart';
import '/utils/api.dart';
import '/providers/user_provider.dart';

class AddProjectDialog extends StatefulWidget {
    @override
    _AddProjectDialog createState() => _AddProjectDialog();
}

class _AddProjectDialog extends State<AddProjectDialog> {
    final _formKey = GlobalKey<FormState>();
    final _nameController = TextEditingController();
    final _descriptionController = TextEditingController();

    void _addProject(BuildContext context){
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        
        API(accessToken).addProject(
            name: _nameController.text,
            description: _descriptionController.text
        ).catchError((error){
            showSnackBar(context, error.message);
        });
    }
    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget formAddProject = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    form(
                        title: 'Name', 
                        controller: _nameController, 
                        message: 'Name is required.',
                        focus: focusNode
                    ),
                    form(
                        title: 'Description', 
                        controller: _descriptionController, 
                        message: 'Description is required.',
                        focus: focusNode
                    ),
                ]
            )
        );

        return AlertDialog(
            title: Text('Add New Project'),
            content: Container(
                child: formAddProject
            ),
            actions: [
                ElevatedButton(
                    child: Text('Add'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            _addProject(context);
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Form validation failed. Check input and try again.');
                        }
                    },
                ),
                ElevatedButton(
                    child: Text('Cancel'),
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                )
            ]
        );
    }
}