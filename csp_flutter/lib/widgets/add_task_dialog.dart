import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/providers/user_provider.dart';

class AddTaskDialog extends StatefulWidget {
    final int? _projectId;
    
    AddTaskDialog([ this._projectId ]);

    @override
    _AddTaskDialog createState() => _AddTaskDialog();
}

class _AddTaskDialog extends State<AddTaskDialog> {
    final _formKey = GlobalKey<FormState>();
    
    final _titleController = TextEditingController();
    final _descriptionController = TextEditingController();

    List<DropdownMenuItem> _assemblyTeamOptions = [];
    int? _assignedTo;

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            API(accessToken).getUsersByDesignation('assembly-teams').then((resolvers) {
                setState(() {
                    _assemblyTeamOptions = resolvers.map((resolver) {
                        return DropdownMenuItem(
                            child: Text(resolver.email!),
                            value: resolver.id
                        );
                    }).toList();
                });
            }).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    void _addTask(BuildContext context){
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        
        API(accessToken).addTask(
            title: _titleController.text,
            description: _descriptionController.text,
            assignedTo: _assignedTo!,
            projectId: widget._projectId!
        ).catchError((error){
            showSnackBar(context, error.message);
        });
    }

    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        DropdownButtonFormField txtAssemblyTeam = DropdownButtonFormField(
            decoration: InputDecoration(labelText: 'Assembly Team'),
            items: _assemblyTeamOptions,
            onChanged: (value) {
                // Update the assignedTo state according to selected option.
                setState(() {
                  _assignedTo = value;
                });
            }
        );

        Widget formAddTask = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    form(
                        title: 'Title', 
                        controller: _titleController, 
                        message: 'Title is required.',
                        focus: focusNode
                    ),
                    form(
                        title: 'Description', 
                        controller: _descriptionController, 
                        message: 'Description is required.',
                        focus: focusNode
                    ),
                    txtAssemblyTeam
                ]
            )
        );

        return AlertDialog(
            title: Text('Add New Task'),
            content: Container(
                child: SingleChildScrollView(
                    child: formAddTask
                )
            ),
            actions: [
                ElevatedButton(
                    child: Text('Add'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            _addTask(context);
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Form validation failed. Check input and try again.');
                        }
                    }
                ),
                ElevatedButton(
                    child: Text('Cancel'),
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                ),
            ],
        );
    }
}