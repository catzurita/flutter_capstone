import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/providers/user_provider.dart';
import '/screens/about_the_developer.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
       
        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    SizedBox(height: 36.0),
                                    Container(
                                        height: 42,
                                        child: Image.asset(
                                        'assets/ffuf-logo.png',
                                        color: Colors.white,
                                    )), // Modify width and color of Image widget to match given sample.
                                    Container(
                                        margin: EdgeInsets.only(top: 16.0),
                                        child: Column(
                                            children: [
                                                Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text('FFUF Project Management',
                                                        textAlign: TextAlign.left, 
                                                        style: TextStyle(
                                                            fontSize: 20.0, 
                                                            color: Colors.white,
                                                        )
                                                    )
                                                )
                                                ,
                                                Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text('Made by FFUF Internal Dev Team',
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                            color: Colors.white
                                                        )
                                                    )
                                                )
                                                ,
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                ListTile(
                                    // contentPadding: EdgeInsets.only(bottom: 0),
                                    dense: true,
                                    visualDensity: VisualDensity(vertical: -4),
                                    title: Text(
                                        'About the Developer',
                                        style: TextStyle(
                                            color: Colors.white
                                        )
                                    ),
                                    onTap: (){
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => AboutMe()),
                                        );
                                    },
                                ),
                                ListTile(
                                    dense: true,
                                    visualDensity: VisualDensity(vertical: -4),
                                    title: Text(
                                        'Logout', 
                                        style: TextStyle(
                                            color: Colors.white
                                        )
                                    ),
                                    onTap: () async {
                                        
                                        Provider.of<UserProvider>(context, listen:false).setAccessToken(null);
                                        Provider.of<UserProvider>(context, listen:false).setDesignation(null);

                                        SharedPreferences prefs = await SharedPreferences.getInstance();
                                
                                        prefs.remove('accessToken');
                                        prefs.remove('userId');

                                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                                    }
                                )
                            ]
                        )
                    ]
                )
            )
        );
    }
}