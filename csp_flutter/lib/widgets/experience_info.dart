import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ExperienceInfo extends StatelessWidget {

    final String company;
    final String position;
    final String duration;

    ExperienceInfo({ 
        required this.company, 
        required this.position, 
        required this.duration}
    );

    @override
    Widget build(BuildContext context) {
        return ListTile(
            leading: Padding(
                padding: EdgeInsets.only(
                    top: 8, 
                    left: 20
                ),
                child: Icon(
                    FontAwesomeIcons.solidCircle, 
                    size:12,
                    color:  Color.fromRGBO(20, 45, 68, 1)
                )
            ),
            title: Text(
                company,
                style: TextStyle(
                    color:  Color.fromRGBO(20, 45, 68, 1),
                    fontWeight: FontWeight.bold
                )
            ),
            subtitle: Text(
                '$position ($duration)',
                style: TextStyle(
                    fontFamily: 'Raleway',
                    color:  Color.fromRGBO(20, 45, 68, 1)
                )
            )
        );
    }
}

