import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/project.dart';
import '/providers/user_provider.dart';
import '/widgets/assign_project_dialog.dart';
import '/screens/task_list_screen.dart';

class ProjectCard extends StatefulWidget {
    final Project _project;
    final Function _reloadProjects;

    ProjectCard(this._project, this._reloadProjects);

    @override
    _ProjectCard createState() => _ProjectCard();
}

class _ProjectCard extends State<ProjectCard> {    

    @override
    Widget build(BuildContext context) {
        Project project = widget._project;
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget ltProjectInfo = Align (
            alignment: Alignment.centerLeft,
            child:ListTile(
                title: Text(
                    project.name!,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black
                    ),
                ),
                subtitle: Wrap(
                    direction: Axis.vertical, 
                    children: [
                        Text(project.description!)
                    ]
                )
            )
        );

        Widget btnAssign = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Assign'),
                onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => AssignProjectDialog(project.id)
                    ).then((value) {
                        setState(() {
                            widget._reloadProjects();
                        });  
                    });
                },
            )
        );

        Container btnTasks = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Tasks'),
                onPressed: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskListScreen(project.id)));
                    widget._reloadProjects();
                },
            )
        );

        List<Widget> buttons(){
            return (designation == 'contractor' && project.assignedTo == null) ?
                [btnAssign, btnTasks]
                :
                [btnTasks];
        }

        Row rowActions = Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
                 ...buttons()
            ]
        );
        
        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        ltProjectInfo,
                        rowActions
                    ]
                )
            )
        );
    }
}