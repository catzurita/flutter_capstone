import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}
class _TaskCard extends State<TaskCard> {    
    String? taskStatus;
    
    @override
    void initState(){
        super.initState();
        taskStatus = widget._task.status!;
        print('here');
    }

    void updateStatus(BuildContext context, newStatus){
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        
        API(accessToken).updateTaskStatus(
            taskId: widget._task.id,
            status: newStatus
        ).then((value){
            if(value == true){
                setState(() => taskStatus = newStatus);
            }
        }).catchError((error){
            showSnackBar(context, error.message);
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        // Modify both the main and cross axis alignment.
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                widget._task.title,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black
                                ),
                            ),
                            Text(widget._task.description)
                        ]
                    )
                ),
                Chip(label: Text(taskStatus!))
            ]
        );

        Widget buttonTaskCard(String text, String status){
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 4),
                child: ElevatedButton(
                    child: Text(text),
                    onPressed: () { 
                        updateStatus(context, status);
                    },
                )
            );
        }

        Widget btnReject = Container( 
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    onPrimary: Colors.black,
                    side: BorderSide(
                        color: Colors.black,
                        width: 1
                    )
                ),
                child: Text('Reject'),
                onPressed: () { 
                    updateStatus(context, 'Rejected');   
                },
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text(
                    'Detail',
                    style: TextStyle(
                        decoration: TextDecoration.underline 
                    ),
                ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        List<Widget> actionButtons(){
            if(designation == 'contractor'){
                if (taskStatus == 'Completed'){
                    return [
                        buttonTaskCard('Accept', 'Accepted'), 
                        btnReject
                    ];
                } else if (taskStatus == 'Rejected'){
                    return [buttonTaskCard('Accept', 'Accepted')];
                } else if (taskStatus == 'Accepted'){
                    return [btnReject];
                }
            } else if (designation == 'assembly-team'){
                if (taskStatus == 'Pending'){
                    return [buttonTaskCard('Start', 'Ongoing')];
                } else if (taskStatus == 'Ongoing'){
                    return [buttonTaskCard('Finish', 'Completed')];
                }
            } 
            return [];
        }

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),
                ...actionButtons(),
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}